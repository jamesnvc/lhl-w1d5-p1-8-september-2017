//
//  Player.h
//  DynamismDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject

@property (nonatomic,assign,readonly) NSInteger health;

- (void)fight;
- (void)currentStatus;

@end
