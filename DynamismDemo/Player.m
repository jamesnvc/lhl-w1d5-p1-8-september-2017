//
//  Player.m
//  DynamismDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Player.h"

@interface Player ()
@property (nonatomic,assign) NSInteger fightTimes;
@end

@implementation Player

- (instancetype)init
{
    self = [super init];
    if (self) {
        _health = 100;
    }
    return self;
}

- (void)fight
{
    _health -= 1;
    self.fightTimes += 1;
    [self currentStatus];
}

- (void)currentStatus
{
    NSLog(@"Player has %ld health & fought %ld times", self.health, self.fightTimes);
}

@end
