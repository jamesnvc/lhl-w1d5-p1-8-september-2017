//
//  main.m
//  DynamismDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Emoji.h"
#import "OurString.h"
#import "Player.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *a1 = @[@"foo", @"bar"];
        NSArray *a2 = [NSMutableArray arrayWithArray:@[@"baz", @"quux"]];

        SEL addObjectSel = @selector(addObject:);
        for (NSArray *a in @[a1, a2]) {
             if ([a respondsToSelector:addObjectSel]) {
                 [a performSelector:addObjectSel withObject:@"xxx"];
//                 [a addObject:@"xxx"];
                 [a performSelector:addObjectSel withObject:@"zzz"];
//                 [a addObject:@"zzz"];
             }
        }


        NSLog(@"array 1 = %@", a1);
        NSLog(@"array 2 = %@", a2);

        NSLog(@"string = %@ ,  %@", [@"Hello" makeLit], [NSString poop]);
//        NSLog(@"string = %@ ,  %@", [[[OurString alloc] initWithString:@"Hello"] makeLit], [OurString poop]);

        /*
        [a2 performSelector:addObjectSel withObject:@"yyy" afterDelay:3];
        NSLog(@"array 2 = %@", a2);
        [[NSRunLoop currentRunLoop] run];
         */


        Player *p = [[Player alloc] init];
        [p fight];
        [p fight];
//        p.fightTimes = 9999;
//        [p performSelector:@selector(setFightTimes:) withObject:@9999];
        [p currentStatus];
        NSLog(@"checking health: %ld", p.health);
    }
    return 0;
}
