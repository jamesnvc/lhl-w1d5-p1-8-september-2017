//
//  OurString.h
//  DynamismDemo
//
//  Created by James Cash on 08-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OurString : NSString
@property NSInteger foo;

- (OurString*)makeLit;

+ (OurString*)poop;

@end
